package com.fromtheseventhsky.testapp.ubanktestapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FromTheSeventhSky in 2017.
 */

public class TestAdapter extends RecyclerView.Adapter<TestAdapter.TestHolder> {

    private ArrayList<String> mDynamicNames;
    private final String mStaticName;

    private int mMaxWidth;

    TestAdapter() {
        mDynamicNames = new ArrayList<>();
        fillList();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm", Locale.getDefault());
        mStaticName = format.format(new Date());
        mMaxWidth = 0;
    }

    private void fillList() {
        mDynamicNames.add("0");
        for (int i = 1; i < 100; i++) {
            mDynamicNames.add(mDynamicNames.get(i - 1).concat(String.format(" %s", i)));
        }
    }

    @Override
    public TestHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_test, parent, false);
        return new TestHolder(v);
    }

    @Override
    public void onBindViewHolder(TestHolder holder, int position) {
        holder.initView(mDynamicNames.get(position));
    }

    @Override
    public int getItemCount() {
        return mDynamicNames.size();
    }

    class TestHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.textWithGrow)
        TextView mTextViewWithGrow;

        @BindView(R.id.textWithoutGrow)
        TextView mTextViewWithoutGrow;

        private int mTextViewWithoutGrowWidth;
        private int mMarginsSum;

        TestHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            mTextViewWithoutGrowWidth = 0;
            mMarginsSum = 0;

            setTextAndCalculateWidth();
            calculateMarginsSum();
            setMaxWidthForViewWithGrow();
        }

        void initView(String text) {
            mTextViewWithGrow.setText(text);
        }

        /**
         * Присваивает текст полю, после чего измеряет его ширину
         */
        private void setTextAndCalculateWidth() {
            mTextViewWithoutGrow.setText(mStaticName);
            mTextViewWithoutGrow.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            mTextViewWithoutGrowWidth = mTextViewWithoutGrow.getMeasuredWidth();
        }

        /**
         * Считает максимальную ширину поля, в котором текст меняется в зависимости от позиции
         */
        private void setMaxWidthForViewWithGrow() {
            if (mMaxWidth == 0) {
                itemView.post(new Runnable() {
                    @Override
                    public void run() {
                        if (mMaxWidth == 0) {
                            int rootWidth = getWidthWithoutPadding(itemView);
                            mMaxWidth = rootWidth - (mMarginsSum + mTextViewWithoutGrowWidth);
                        }
                        mTextViewWithGrow.setMaxWidth(mMaxWidth);
                    }
                });
            } else {
                mTextViewWithGrow.setMaxWidth(mMaxWidth);
            }
        }

        /**
         * Считает ширину контейнера за вычитом паддингов
         */
        private int getWidthWithoutPadding(View v) {
            return v.getWidth() - v.getPaddingRight() - v.getPaddingLeft();
        }

        /**
         * Считает сумму отступов обоих текстовых полей
         */
        private void calculateMarginsSum() {
            if (mMarginsSum == 0) {
                ViewGroup.MarginLayoutParams textViewWithGrowLP =
                        ((ViewGroup.MarginLayoutParams) mTextViewWithGrow.getLayoutParams());
                ViewGroup.MarginLayoutParams textViewWithoutGrowLP =
                        ((ViewGroup.MarginLayoutParams) mTextViewWithoutGrow.getLayoutParams());

                mMarginsSum = textViewWithGrowLP.leftMargin + textViewWithGrowLP.rightMargin +
                        textViewWithoutGrowLP.leftMargin + textViewWithoutGrowLP.rightMargin;
            }
        }
    }
}
